# Features

- Traefik 3.x (for older versions look at release/x branch)
- LetsEncrypt Certification generation
- Wildcard HTTP to HTTPS Redirection
- HTTPS Support
- Webapplication whoami to see if docker-compose.yml setup is working
- .env support
- IP Allowlisting
- Minimum TLS 1.2 Support
